#############################
#Tomas Lind 2016
#http://tomaslind.net/2016/06/14/toggl-time-entries-sql-server/
#############################
 
#############################
#Credits to
#http://powerbits.com.au/blog/2015/7/3/use-powershell-to-access-the-toggl-api
#for how to get the "detail report" via PowerShell
#############################
 
cls
Import-Module SQLPS
 
#############################
#Settings:
$Instance = "$(ESCAPE_DQUOTE(SRVR))" #"vmtravis" #Update this when placing the 
$Email = "tpoll@cmnhospitals.org"
$WorkspaceId = 1507708
$user = "7d1ad5b767854ff22d016c5d723a0d8a"
$NoDaysHistory = 30
#############################
 
$DatabaseName = "TogglDB"
$ConnString = "Server=$Instance;Database=$DatabaseName;Integrated Security=SSPI;"
 
$pass = "api_token"
$pair = "$($user):$($pass)"
#$pair = "$($user):$($pass)"
$bytes = [System.Text.Encoding]::ASCII.GetBytes($pair)
$base64 = [System.Convert]::ToBase64String($bytes)
$basicAuthValue = "Basic $base64"
$headers = @{ Authorization = $basicAuthValue }
$contentType = "application/json"
 
$cmd = "SELECT CAST(CAST(ISNULL(MAX(updated), DATEADD(YEAR, -1, GETDATE())) AS DATE) AS CHAR(10)) AS MaxUpdated FROM TogglDB.dbo.DetailedReport"
$MaxUpdated = Invoke-Sqlcmd -Query $cmd -ServerInstance ($Instance) | select -expand MaxUpdated
 
$Since = (get-date).AddDays(-$NoDaysHistory).ToString("yyyy-MM-dd")
 
$uriReport = "https://toggl.com/reports/api/v2/details?user_agent="+$Email + "&workspace_id="+$WorkspaceId
$TogglResponse = Invoke-RestMethod -Uri $uriReport -Headers $headers -ContentType $contentType
$responseTotal = $TogglResponse.total_count
$pageNum = 1
$DetailReport = @()
 
while ($responseTotal -gt 0)
{
    $TogglResponse = Invoke-RestMethod -Uri $uriReport+"&page="$pageNum+"&since="$Since -Headers $headers -ContentType $contentType
    $TogglResponseData = $TogglResponse.data
    $DetailReport += $TogglResponseData | Where-Object {$_.updated -ge $MaxUpdated}
    $responseTotal = $responseTotal - 1
    $pageNum++
}
 
#Uncomment to see report contents
#$DetailReport | Format-Table -AutoSize | Out-String -Width 4096
 
$DetailReportTable = New-Object System.Data.DataTable
$DetailReportTable.Columns.Add("id", "System.Int32") | Out-Null
$DetailReportTable.Columns.Add("pid", "System.Int32") | Out-Null
$DetailReportTable.Columns.Add("project", "System.String") | Out-Null
$DetailReportTable.Columns.Add("client", "System.String") | Out-Null
$DetailReportTable.Columns.Add("tid", "System.Int32") | Out-Null
$DetailReportTable.Columns.Add("task", "System.String") | Out-Null
$DetailReportTable.Columns.Add("uid", "System.Int32") | Out-Null
$DetailReportTable.Columns.Add("user", "System.String") | Out-Null
$DetailReportTable.Columns.Add("description", "System.String") | Out-Null
$DetailReportTable.Columns.Add("start", "System.DateTime") | Out-Null
$DetailReportTable.Columns.Add("end", "System.DateTime") | Out-Null
$DetailReportTable.Columns.Add("dur", "System.Int64") | Out-Null
$DetailReportTable.Columns.Add("updated", "System.DateTime") | Out-Null
$DetailReportTable.Columns.Add("use_stop", "System.Boolean") | Out-Null
$DetailReportTable.Columns.Add("is_billable", "System.Boolean") | Out-Null
$DetailReportTable.Columns.Add("billable", "System.Single") | Out-Null
$DetailReportTable.Columns.Add("cur", "System.String") | Out-Null
$DetailReportTable.Columns.Add("tags", "System.String") | Out-Null
 
foreach ($Row in $DetailReport)
{
    $DetailReportTable.Rows.Add($Row.id, $Row.pid, $Row.project, $Row.client, $Row.tid, $Row.task, $Row.uid, $Row.user, $Row.description, $Row.start, $Row.end, $Row.dur, $Row.updated, $Row.use_stop, $Row.is_billable, $Row.billable, $Row.cur, $Row.tags -join ",") | Out-Null
}
 
$conn = New-Object System.Data.SqlClient.SqlConnection $ConnString
$conn.Open()
 
$query = "dbo.SaveDetailedReport"
$cmd = New-Object System.Data.SqlClient.SqlCommand
$cmd.Connection = $conn
$cmd.CommandType = [System.Data.CommandType]"StoredProcedure"
$cmd.CommandText = $query
$cmd.Parameters.Add("@DetailedReport", [System.Data.SqlDbType]::Structured) | Out-Null
$cmd.Parameters["@DetailedReport"].Value = $DetailReportTable
 
$cmd.ExecuteNonQuery() | Out-Null
$conn.Close()