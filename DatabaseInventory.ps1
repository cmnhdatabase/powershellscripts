$Network = ("azuresqlserver.childrensmiraclenetwork.org", "sqldev01", "sqltest01", "sqlstaging01", "sqlserver01", "sqlrpt01", "sqlserver", "sqlserver2")
$InventoryServer = "azuresqlserver.childrensmiraclenetwork.org"

foreach ($ServerName in $Network) {
    Write-Host ("Attempting to connect to " + $ServerName)
    Enter-PSSession -ComputerName $ServerName
    Write-Host ("Connected to " + $ServerName + "`n")
    $Major = [environment]::OSVersion.Version.Major
    $Minor = [environment]::OSVersion.Version.Minor
    $Build = [environment]::OSVersion.Version.Build
    $Revision = [environment]::OSVersion.Version.Revision
    $OSVersion = "$Major.$Minor.$Build.$Revision"
    $SQLQuery = "SELECT
                @@SERVERNAME ServerName,
                @@VERSION SQLVersion,
                RIGHT(@@version, LEN(@@version)- 3 -charindex (' ON ', @@VERSION)) OSName,
                '$OSVersion' OSVersion,
                serverproperty('Edition') Licensing,
                CAST(ROUND(physical_memory_kb / 1024.00,0) AS INT) MemoryMB,
                CAST(ROUND(physical_memory_kb / 1024.00 / 1024.00,0) AS INT) MemoryGB,
                cpu_count Cores,
                cpu_count LicensedCores,
                sqlserver_start_time LastUpdated,
                GETDATE() LastModified
                FROM
                sys.dm_os_sys_info"

    $QueryResults = Invoke-Sqlcmd2 -query $SQLQuery -ServerInstance $ServerName
    
    Exit-PSSession
    $QueryResults.Table
    if (-not ([string]::IsNullOrEmpty($QueryResults))) {
        Invoke-Sqlcmd2 -Query ("MERGE SqlManagement.dbo.SQLInventory t
        USING (SELECT
                '$($QueryResults.ServerName)' ServerName,
                '$($QueryResults.SQLVersion)' SQLVersion,
                '$($QueryResults.OSName)' OSName,
                '$($QueryResults.OSVersion)' OSVersion,
                '$($QueryResults.Licensing)' Licensing,
                '$($QueryResults.MemoryMB)' MemoryMB,
                '$($QueryResults.MemoryGB)' MemoryGB,
                '$($QueryResults.Cores)' Cores,
                '$($QueryResults.LicensedCores)' LicensedCores,
                '$($QueryResults.LastUpdated)' LastUpdated,
                '$($QueryResults.LastModified)' LastModified
        ) s ON s.ServerName = t.ServerName
        WHEN MATCHED THEN
        UPDATE SET
        t.SQLVersion = s.SQLVersion,
        t.OSName = s.OSName,
        t.OSVersion = s.OSVersion,
        t.Licensing = s.Licensing,
        t.MemoryMB = s.MemoryMB,
        t.MemoryGB = s.MemoryGB,
        t.Cores = s.Cores,
        t.LicensedCores = s.LicensedCores,
        t.LastUpdated = s.LastUpdated,
        t.LastModified = s.LastModified
        WHEN NOT MATCHED THEN
        INSERT
        (
        ServerName,
        SQLVersion,
        OSName,
        OSVersion,
        Licensing,
        MemoryMB,
        MemoryGB,
        Cores,
        LicensedCores,
        LastUpdated,
        LastModified
        )
        VALUES
        (
        s.ServerName,
        s.SQLVersion,
        s.OSName,
        s.OSVersion,
        s.Licensing,
        s.MemoryMB,
        s.MemoryGB,
        s.Cores,
        s.LicensedCores,
        s.LastUpdated,
        s.LastModified
        );") -ServerInstance $InventoryServer
    }
}

