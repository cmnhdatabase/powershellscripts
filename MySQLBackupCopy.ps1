$folder1 = "\\AzureMySQL\c$\MySQLBackupHome\backup"
$folder2 = "\\cl1-cifs\SQLBackup\AzureMySQL"
# Get all files under $folder1, filter out directories
$firstFolder = Get-ChildItem -Recurse $folder1 | Where-Object { -not $_.PsIsContainer }

$firstFolder | ForEach-Object {

# Check if the file, from $folder1, exists with the same path under $folder2
If (-Not ( Test-Path ( $_.FullName.Replace($folder1, $folder2) ) )) {
New-Item -ItemType File -Path $_.FullName.Replace($folder1, $folder2) -Force
Copy-Item $_.FullName $_.FullName.Replace($folder1, $folder2) -Force
}}